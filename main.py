#!/usr/bin/python

import sys, os, pyperclip, pysftp, webbrowser, random, string
from shutil import copyfile
from random import randint

# Directory to place files with their random name before uploading
LOCAL_DIR = "/home/ruben/Pictures/shared/"

NOT_MSG = "Uploaded file to i.rubenwardy.com"

# Remote server details
REMOTE_HOST = "rubenwardy.com"
REMOTE_USER = "pictures"
REMOTE_DEST = "/var/www/pictures"
REMOTE_URL = "https://i.rubenwardy.com/"

def getUploadName(path):
	filename = os.path.basename(path)
	filename, file_extension = os.path.splitext(path)
	return "".join(random.choice(string.ascii_lowercase +string.ascii_uppercase + string.digits) for _ in range(5)) + file_extension

def uploadFile(path):
	# Rename file
	print("Preparing for upload...")
	newFilename = getUploadName(path)
	newPath = LOCAL_DIR + newFilename
	copyfile(path, newPath)

	# Upload using SFTP
	print("Connecting...")
	srv = pysftp.Connection(host=REMOTE_HOST, username=REMOTE_USER, log="/tmp/pysftp.log")

	print("Uploading " + newPath + "...")
	with srv.cd(REMOTE_DEST):
		srv.put(newPath)
	srv.close()

	return REMOTE_URL + newFilename

try:
	path = sys.argv[1]
	url = uploadFile(path)
	if url is not None:
		# Clipboard
		print("Copying to clipboard...")
		pyperclip.copy(url)

		# Open web browser
		webbrowser.open(url)

		# Send notification
		os.system("notify-send \"Ruben Image Upload\" \"" + NOT_MSG + "\"")

except IndexError:
	print("USAGE: rubenimageupload /path/to/file")
	sys.exit(1)
