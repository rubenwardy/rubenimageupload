# Ruben Image Upload

A very light weight solution for uploading images to a web server with a random
name.

License: MIT

## Example

* `rubenimageupload path/to/image.png`
* Copies image.png to a tmp dir, and renames it to something like `sR6ZL.png`.
* Logs into remote server, CDs to the correct directory, then uploads the file.
* The remote server then serves this image at `i.example.com/sR6ZL.png`.
* Sends a notification, copies URL to clipboard, and opens URL in browser.

## Usage

* Git clone this repo somewhere.
* Change variables in main.py near the top.
* Make sure you have a SSH key installed for the remote server for that user.
* Run `sudo pip install -r requirements.txt`
* Install xclip and xsel - these are CLIs to X's paste buffers, and will 
not conflict with managers such as Clipman. See this wiki page for more 
info: https://wiki.archlinux.org/index.php/clipboard
* Run `sudo make install`
* Open any images with `rubenimageupload` to upload it.
  XFCE allows you to do this from the screenshot tool.
